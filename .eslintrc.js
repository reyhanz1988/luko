module.exports = {
	env: {
		es2021: true,
		node: true,
		"jest/globals": true,
	},
	extends: [
		"eslint:recommended",
		"plugin:react/recommended",
		"plugin:@typescript-eslint/recommended",
		"plugin:jest/recommended",
		"plugin:react/jsx-runtime",
		"prettier",
	],
	parser: "@typescript-eslint/parser",
	parserOptions: {
		ecmaFeatures: {
			jsx: true,
		},
		ecmaVersion: "latest",
		sourceType: "module",
	},
	globals: {
		fetch: false,
	},
	plugins: ["react", "@typescript-eslint"],
	rules: {
		"react/jsx-uses-react": "error",
		"react/jsx-uses-vars": "error",
		indent: "off",
		quotes: ["error", "double"],
		semi: ["error", "always"],
	},
	settings: {
		react: {
			version: "detect",
		},
	},
};
