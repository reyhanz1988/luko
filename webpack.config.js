// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require("path");

// eslint-disable-next-line @typescript-eslint/no-var-requires
const webpack = require("webpack");
// eslint-disable-next-line @typescript-eslint/no-var-requires
const HtmlWebpackPlugin = require("html-webpack-plugin");

const appDirectory = path.resolve(__dirname);
// eslint-disable-next-line @typescript-eslint/no-var-requires
const { presets } = require(`${appDirectory}/babel.config.js`);

const compileNodeModules = [
	// Add every react-native package that needs compiling
	// 'react-native-gesture-handler',
].map((moduleName) => path.resolve(appDirectory, `node_modules/${moduleName}`));

const babelLoaderConfiguration = {
	test: /\.js$|tsx?$/,
	// Add every directory that needs to be compiled by Babel during the build.
	include: [
		path.resolve(appDirectory, "index.rnvi.js"), // Entry to your application
		path.resolve(appDirectory, "index.web.js"), // Entry to your application
		path.resolve(appDirectory, "App.tsx"), // Change this to your main App file
		path.resolve(appDirectory, "src"),
		...compileNodeModules,
		path.resolve(appDirectory, "node_modules/react-native-vector-icons"),
		path.resolve(appDirectory, "node_modules/react-native-paper"),
	],
	use: {
		loader: "babel-loader",
		options: {
			cacheDirectory: true,
			presets: ["module:metro-react-native-babel-preset"],
			plugins: ["react-native-web"],
		},
	},
};

const imageLoaderConfiguration = {
	test: /\.(gif|jpe?g|png|svg)$/,
	use: {
		loader: "url-loader",
		options: {
			name: "[name].[ext]",
		},
	},
};

const RNVIConfiguration = {
	test: /\.ttf$/,
	loader: "url-loader", // or directly file-loader
	include: path.resolve(appDirectory, "node_modules/react-native-vector-icons"),
};

module.exports = {
	/*entry: {
		app: path.join(__dirname, "index.web.js"),
	},*/
	entry: [path.join(appDirectory, "index.rnvi.js"), path.join(appDirectory, "index.web.js")],
	output: {
		filename: "bundle.web.js",
		path: path.resolve(appDirectory, "dist"),
	},
	resolve: {
		extensions: [".web.tsx", ".web.ts", ".tsx", ".ts", ".web.js", ".js"],
		alias: {
			"react-native$": "react-native-web",
		},
	},
	module: {
		rules: [babelLoaderConfiguration, imageLoaderConfiguration, RNVIConfiguration],
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: path.join(appDirectory, "index.html"),
		}),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.DefinePlugin({
			// See: https://github.com/necolas/react-native-web/issues/349
			__DEV__: JSON.stringify(true),
		}),
	],
};
