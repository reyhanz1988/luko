module.exports = [
	/*0*/
	{
		en: "Home",
		fr: "Accueil",
	},

	/*1*/
	{
		en: "Power",
		fr: "Pouvoir",
	},

	/*2*/
	{
		en: "Gas",
		fr: "Gaz",
	},

	/*3*/
	{
		en: "News",
		fr: "Des nouvelles",
	},

	/*4*/
	{
		en: "Save up to €15,000",
		fr: "Économisez jusqu'à €15,000",
	},

	/*5*/
	{
		en: "Find the price of your mortgage insurance in 5 minutes",
		fr: "Trouvez le prix de votre assurance de prêt immobilier en 5 minutes",
	},

	/*6*/
	{
		en: "Get your price",
		fr: "Obtenez votre prix",
	},

	/*7*/
	{
		en: "Realty",
		fr: "Immobilier",
	},

	/*8*/
	{
		en: "Find your future home match. Discover Immo, Luko`s home search feature - à la Tinder",
		fr: "Trouvez votre futur match à domicile. Découvrez Immo, la fonction de recherche de logement de Luko - à la Tinder",
	},

	/*9*/
	{
		en: "Discover Realty",
		fr: "Découvrez l'immobilier",
	},

	/*10*/
	{
		en: "Gas with Gazpar",
		fr: "Gaz avec Gazpar",
	},

	/*11*/
	{
		en: "Power with Linky",
		fr: "Puissance avec Linky",
	},

	/*12*/
	{
		en: "Luko Water",
		fr: "Eau Luko",
	},
];
