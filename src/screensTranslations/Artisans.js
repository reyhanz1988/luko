module.exports =  [
/*0*/
{
    en: "Artisans",
    fr: "Artisans",

},

/*1*/
{
    en: "Ask for advice",
    fr: "Demander conseil",

},

/*2*/
{
    en: "20 min of tele-advices for your home",
    fr: "20 min de télé-conseils pour votre domicile",

},

/*3*/
{
    en: "Access to artisans",
    fr: "Accès aux artisans",

},

/*4*/
{
    en: "Insurance from €4/month",
    fr: "Assurance à partir de €4/mois",

},

/*5*/
{
    en: "Tutorials",
    fr: "Tutoriels",

},
];