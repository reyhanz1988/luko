module.exports =  [
/*0*/
{
    en: "Menu",
    fr: "Menu",

},

/*1*/
{
    en: "Profile",
    fr: "Profil",

},

/*2*/
{
    en: "Settings",
    fr: "Réglages",

},

/*3*/
{
    en: "Perks",
    fr: "Avantages",

},

/*4*/
{
    en: "FAQ",
    fr: "FAQ",

},

/*5*/
{
    en: "Privacy Policy",
    fr: "Politique de confidentialité",

},

/*6*/
{
    en: "Login",
    fr: "Connexion",

},

/*7*/
{
    en: "Logout",
    fr: "Se déconnecter",

},
];