module.exports = [
	/*0*/
	{
		en: "Language",
		fr: "Langue",
	},

	/*1*/
	{
		en: "Email",
		fr: "E-mail",
	},

	/*2*/
	{
		en: "Password",
		fr: "Mot de passe",
	},

	/*3*/
	{
		en: "Login",
		fr: "Connexion",
	},
];
