module.exports = [
	/*0*/
	{
		en: "Realty",
		fr: "Immobilier",
	},

	/*1*/
	{
		en: "View More",
		fr: "Voir Plus",
	},

	/*2*/
	{
		en: "Search",
		fr: "Chercher",
	},

	/*3*/
	{
		en: "Price",
		fr: "Prix",
	},

	/*4*/
	{
		en: "Min Price",
		fr: "Prix minimum",
	},

	/*5*/
	{
		en: "Max Price",
		fr: "Prix maximum",
	},

	/*6*/
	{
		en: "Beds",
		fr: "Des lits",
	},

	/*7*/
	{
		en: "Min Beds",
		fr: "Lits minimum",
	},

	/*8*/
	{
		en: "Max Beds",
		fr: "Lits maximum",
	},

	/*9*/
	{
		en: "Filter",
		fr: "Filtre",
	},

	/*10*/
	{
		en: "Reset",
		fr: "Réinitialiser",
	},

	/*11*/
	{
		en: "Close",
		fr: "Fermer",
	},

	/*12*/
	{
		en: "Advance Search",
		fr: "Recherche Avancée",
	},
];
