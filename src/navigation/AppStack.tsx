import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import HomeScreen from "../screens/HomeScreen";
import InsuranceScreen from "../screens/InsuranceScreen";
import ArtisansScreen from "../screens/ArtisansScreen";
import RealtiesScreen from "../screens/RealtiesScreen";
import MenuScreen from "../screens/MenuScreen";
import LoginScreen from "../screens/LoginScreen";

const Stack = createStackNavigator();

const HomeNav = () => {
	return (
		<Stack.Navigator screenOptions={{ headerShown: false }}>
			<Stack.Screen name="Home" component={HomeScreen} />
		</Stack.Navigator>
	);
};
export { HomeNav };

const InsuranceNav = () => {
	return (
		<Stack.Navigator screenOptions={{ headerShown: false }}>
			<Stack.Screen name="Insurance" component={InsuranceScreen} />
		</Stack.Navigator>
	);
};
export { InsuranceNav };

const ArtisansNav = () => {
	return (
		<Stack.Navigator screenOptions={{ headerShown: false }}>
			<Stack.Screen name="Artisans" component={ArtisansScreen} />
		</Stack.Navigator>
	);
};
export { ArtisansNav };

const RealtiesNav = () => {
	return (
		<Stack.Navigator screenOptions={{ headerShown: false }}>
			<Stack.Screen name="Realties" component={RealtiesScreen} />
		</Stack.Navigator>
	);
};
export { RealtiesNav };

const MenuNav = () => {
	return (
		<Stack.Navigator screenOptions={{ headerShown: false }}>
			<Stack.Screen name="Menu" component={MenuScreen} />
			<Stack.Screen name="Login" component={LoginScreen} />
		</Stack.Navigator>
	);
};
export { MenuNav };
