import React, { useContext } from "react";
import { LangContext } from "../../App";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { HomeNav, InsuranceNav, ArtisansNav, RealtiesNav, MenuNav } from "./AppStack";
import { FIRST_COLOR, SECOND_COLOR } from "@env";
import languages from "../screensTranslations/Navigation";

const Tab = createBottomTabNavigator();

const AppNav = () => {
	const { currentLang } = useContext(LangContext);
	return (
		<Tab.Navigator
			initialRouteName="HomeNav"
			screenOptions={{
				tabBarActiveTintColor: FIRST_COLOR,
				tabBarInactiveTintColor: "#666",
				headerShown: false,
				tabBarItemStyle: {
					backgroundColor: SECOND_COLOR,
					marginBottom: 5,
				},
				tabBarStyle: [{ display: "flex", height: 60 }, null],
			}}>
			<Tab.Screen
				name="HomeNav"
				component={HomeNav}
				options={{
					tabBarTestID: "HomeNav",
					tabBarLabel: languages[0][currentLang],
					tabBarLabelStyle: { fontSize: 12 },
					tabBarIcon: ({ color }) => (
						<MaterialCommunityIcons name="home" color={color} size={24} />
					),
				}}
			/>
			<Tab.Screen
				name="InsuranceNav"
				component={InsuranceNav}
				options={{
					tabBarTestID: "InsuranceNav",
					tabBarLabel: languages[1][currentLang],
					tabBarLabelStyle: { fontSize: 12 },
					tabBarIcon: ({ color }) => (
						<MaterialCommunityIcons name="umbrella" color={color} size={24} />
					),
				}}
			/>
			<Tab.Screen
				name="ArtisansNav"
				component={ArtisansNav}
				options={{
					tabBarTestID: "ArtisansNav",
					tabBarLabel: languages[2][currentLang],
					tabBarLabelStyle: { fontSize: 12 },
					tabBarIcon: ({ color }) => (
						<MaterialCommunityIcons name="wrench" color={color} size={24} />
					),
				}}
			/>
			<Tab.Screen
				name="RealtiesNav"
				component={RealtiesNav}
				options={{
					tabBarTestID: "RealtiesNav",
					tabBarLabel: languages[3][currentLang],
					tabBarLabelStyle: { fontSize: 12 },
					tabBarIcon: ({ color }) => (
						<MaterialCommunityIcons name="magnify" color={color} size={24} />
					),
				}}
			/>
			<Tab.Screen
				name="MenuNav"
				component={MenuNav}
				options={{
					tabBarTestID: "MenuNav",
					tabBarLabel: languages[4][currentLang],
					tabBarLabelStyle: { fontSize: 12 },
					tabBarIcon: ({ color }) => (
						<MaterialCommunityIcons name="menu" color={color} size={24} />
					),
				}}
			/>
		</Tab.Navigator>
	);
};
export default AppNav;
