import React, { useContext, useEffect, useState } from "react";
import { LangContext } from "../../App";
import { connect } from "react-redux";
import * as productActions from "../redux/actions/productActions";
import { Dimensions, ScrollView, StyleSheet, View } from "react-native";
import { Appbar, Button, FAB, List, Surface, Text } from "react-native-paper";
import languages from "../screensTranslations/Artisans";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import LangComponent from "../components/LangComponent";
import PropTypes from "prop-types";
import { FIRST_COLOR } from "@env";

const ArtisansScreen = (props) => {
	const { currentLang } = useContext(LangContext);
	const [getDataError, setGetDataError] = useState([]);
	const [getArtisans, setGetArtisans] = useState([]);
	//FIRST LOAD
	useEffect(() => {
		props.getArtisans();
	}, []);
	//PROPS UPDATE
	useEffect(() => {
		if (props.getArtisansRes == "error") {
			setGetDataError(props.getArtisansRes);
		} else {
			if (props.getArtisansRes.status_msg) {
				setGetDataError(props.getArtisansRes.status_msg);
			} else {
				setGetArtisans(props.getArtisansRes);
				setGetDataError("");
			}
		}
	}, [props.getArtisansRes]);
	//RENDER
	if (getDataError != "") {
		props.getArtisans();
	} else {
		//RENDER
		const artisansList = [];
		if (getArtisans.length > 0) {
			for (const i in getArtisans) {
				artisansList.push(
					<List.Item
						key={"artisans_"+i}
						title={getArtisans[i].title[currentLang]}
						titleNumberOfLines={2}
						descriptionNumberOfLines={2}
						description={getArtisans[i].description[currentLang]}
						left={(props) => (
							<List.Icon {...props} icon={getArtisans[i].icon} color={FIRST_COLOR} />
						)}
						right={(props) => (
							<List.Icon {...props} icon={"chevron-right"} color={FIRST_COLOR} />
						)}
						style={{ borderBottomWidth: 1, borderBottomColor: "#ddd" }}
					/>,
				);
			}
		}

		return (
			<View style={styles.wrapper} testID="ArtisansScreen">
				<Appbar.Header>
					<Appbar.Content
						titleStyle={{ color: "#fff", textAlign: "center" }}
						title={languages[0][currentLang]}
					/>
					<LangComponent />
				</Appbar.Header>
				<View style={styles.artisansView}>
					<Surface style={styles.artisansSurface}>
						<MaterialCommunityIcons
							style={styles.artisansTextIcon}
							name="camera"
							size={24}
						/>
						<Text style={styles.artisansText}>{languages[2][currentLang]}</Text>
					</Surface>
					<Surface style={styles.artisansSurface}>
						<MaterialCommunityIcons
							style={styles.artisansTextIcon}
							name="check"
							size={24}
						/>
						<Text style={styles.artisansText}>{languages[4][currentLang]}</Text>
					</Surface>
				</View>
				<View style={styles.artisansButtonsView}>
					<Button
						uppercase={false}
						style={styles.artisansButton}
						mode="contained"
						onPress={() => console.log("Pressed")}>
						{languages[1][currentLang]}
					</Button>
					<Button
						uppercase={false}
						style={styles.artisansButton}
						mode="contained"
						onPress={() => console.log("Pressed")}>
						{languages[3][currentLang]}
					</Button>
				</View>
				<View style={styles.artisansHeaderSelected}>
					<Text style={styles.artisansHeaderText}>{languages[5][currentLang]}</Text>
				</View>
				<FAB
					style={styles.fab}
					icon="chat-processing"
					color={"#fff"}
					onPress={() => console.log("Pressed")}
				/>
				<ScrollView>{artisansList}</ScrollView>
			</View>
		);
	}
};

const styles = StyleSheet.create({
	wrapper: {
		flex: 1,
	},
	artisansView: {
		flexDirection: "row",
		justifyContent: "space-evenly",
		marginTop: Dimensions.get("window").width * 0.05,
	},
	artisansSurface: {
		width: Dimensions.get("window").width * 0.45,
		borderRadius: 10,
		paddingTop: 10,
		paddingBottom: 10,
		alignItems: "center",
	},
	artisansTouch: {
		backgroundColor: FIRST_COLOR,
		paddingTop: 5,
		paddingBottom: 5,
	},
	artisansTextHeader: {
		width: Dimensions.get("window").width * 0.45,
		textAlign: "center",
		fontSize: 16,
		color: "#fff",
	},
	artisansTextIcon: {
		textAlign: "center",
		color: FIRST_COLOR,
	},
	artisansText: {
		width: Dimensions.get("window").width * 0.45,
		textAlign: "center",
		fontSize: 12,
		color: "#222",
		marginTop: 10,
		paddingTop: 10,
	},
	artisansButtonsView: {
		flexDirection: "row",
		justifyContent: "space-evenly",
		marginTop: Dimensions.get("window").width * 0.02,
	},
	artisansButton: {
		width: Dimensions.get("window").width * 0.45,
		backgroundColor: "#5875eb",
	},
	artisansHeaderSelected: {
		backgroundColor: "#2a8c7d",
		padding: 10,
		margin: 10,
		borderRadius: 5,
	},
	artisansHeaderText: {
		color: "#fff",
		fontSize: 16,
		fontWeight: "bold",
		textAlign: "center",
	},
	fab: {
		position: "absolute",
		margin: 16,
		right: 0,
		bottom: 0,
		zIndex: 10,
		backgroundColor: FIRST_COLOR,
	},
});

ArtisansScreen.propTypes = {
	getArtisans: PropTypes.func,
	getArtisansRes: PropTypes.array,
	navigation: PropTypes.shape({
		navigate: PropTypes.func,
	}),
};

type ArtisansScreen = PropTypes.InferProps<typeof propTypes>;

function mapStateToProps(state) {
	return {
		getArtisansRes: state.productReducer.getArtisansRes,
	};
}
const mapDispatchToProps = {
	...productActions,
};
export default connect(mapStateToProps, mapDispatchToProps)(ArtisansScreen);
