import React, { useContext, useEffect, useState } from "react";
import { LangContext } from "../../App";
import { connect } from "react-redux";
import * as productActions from "../redux/actions/productActions";
import { Dimensions, ScrollView, StyleSheet, TouchableOpacity, View } from "react-native";
import { Button, Card, Divider, Text } from "react-native-paper";
import languages from "../screensTranslations/Perks";
import PropTypes from "prop-types";

const PerksScreen = (props) => {
	const { currentLang } = useContext(LangContext);
	const [getDataError, setGetDataError] = useState([]);
	const [getPerks, setGetPerks] = useState([]);
	const [currentPerks, setCurrentPerks] = useState(0);
	const [togglePerks, setTogglePerks] = useState(false);
	//FIRST LOAD
	useEffect(() => {
		props.getPerks();
	}, []);
	//PROPS UPDATE
	useEffect(() => {
		if (props.getPerksRes == "error") {
			setGetDataError(props.getPerksRes);
		} else {
			if (props.getPerksRes.status_msg) {
				setGetDataError(props.getPerksRes.status_msg);
			} else {
				setGetPerks(props.getPerksRes);
				setGetDataError("");
			}
		}
	}, [props.getPerksRes]);
	const changePerks = (nextPerks) => {
		setCurrentPerks(nextPerks);
		setTogglePerks(!togglePerks);
	};
	//RENDER
	if (getDataError != "") {
		props.getPerks();
	} else {
		let perksButton;
		const perksOptions = [];
		let perksOptionsView;
		const perksList = [];
		if (getPerks[currentPerks]) {
			perksButton = (
				<TouchableOpacity
					testID="togglePerks"
					style={styles.perksHeaderSelected}
					onPress={() => setTogglePerks(!togglePerks)}>
					<Text style={styles.perksHeaderText}>
						{getPerks[currentPerks].title[currentLang]}
					</Text>
				</TouchableOpacity>
			);

			if (togglePerks) {
				for (const i in getPerks) {
					let perksOptionsStyle = styles.perksHeader;
					if (i == currentPerks) {
						perksOptionsStyle = styles.perksHeaderSelected;
					}
					perksOptions.push(
						<TouchableOpacity
							testID={"perksOptions_" + i}
							key={"perksOptions_" + i}
							style={perksOptionsStyle}
							onPress={() => changePerks(i)}>
							<Text style={styles.perksHeaderText}>
								{getPerks[i].title[currentLang]}
							</Text>
						</TouchableOpacity>,
					);
				}
				perksOptionsView = (
					<View testID="perksOptions" style={styles.perksOptions}>
						{perksOptions}
					</View>
				);
			}
			for (const i in getPerks[currentPerks].perks) {
				perksList.push(
					<Card key={"perks_" + i} style={styles.perksCard}>
						<Card.Title title={getPerks[currentPerks].perks[i].title} />
						<Divider style={styles.theDivider} />
						<Card.Content>
							<Text style={styles.contentPerks}>
								{getPerks[currentPerks].perks[i].perks[currentLang]}
							</Text>
							<Text style={styles.descriptionPerks}>
								{getPerks[currentPerks].perks[i].description[currentLang]}
							</Text>
						</Card.Content>
						<Card.Actions style={styles.perkActions}>
							<Button
								style={styles.perksButton}
								mode="contained"
								onPress={() => console.log("Pressed")}>
								{languages[1][currentLang]}
							</Button>
						</Card.Actions>
					</Card>,
				);
			}
		}
		return (
			<View style={styles.wrapper} testID="PerksScreen">
				{perksButton}
				{perksOptionsView}
				<ScrollView style={styles.scrollWrapper}>{perksList}</ScrollView>
			</View>
		);
	}
};

PerksScreen.propTypes = {
	getPerks: PropTypes.func,
	getPerksRes: PropTypes.array,
	navigation: PropTypes.shape({
		navigate: PropTypes.func,
	}),
};
type PerksScreen = PropTypes.InferProps<typeof propTypes>;

const styles = StyleSheet.create({
	wrapper: {
		flex: 1,
	},
	perksHeader: {
		flexDirection: "row",
		width: Dimensions.get("window").width * 0.5,
		backgroundColor: "#56a497",
		padding: 10,
		marginTop: 20,
		marginLeft: 20,
		marginRight: 20,
		borderRadius: 5,
	},
	perksHeaderSelected: {
		width: Dimensions.get("window").width * 0.5,
		backgroundColor: "#5875eb",
		padding: 10,
		marginTop: 20,
		marginLeft: 20,
		marginRight: 20,
		borderRadius: 5,
	},
	perksOptions: {
		backgroundColor: "#a6d3cc",
		paddingBottom: 20,
		borderRadius: 5,
		position: "absolute",
		zIndex: 10,
	},
	scrollWrapper: {
		padding: 20,
		marginBottom: 20,
	},
	perksHeaderText: {
		color: "#fff",
		fontSize: 16,
		fontWeight: "bold",
	},
	perksHeaderIcon: {
		color: "#fff",
		fontSize: 16,
		fontWeight: "bold",
	},
	perksCard: {
		marginBottom: 20,
	},
	theDivider: {
		backgroundColor: "#a6d3cc",
		height: 2,
		marginLeft: 10,
		marginRight: 10,
		marginBottom: 10,
	},
	contentPerks: {
		color: "#666",
		fontWeight: "bold",
		fontSize: 16,
		marginBottom: 10,
	},
	descriptionPerks: {
		color: "#666",
		textAlign: "justify",
	},
	perkActions: {
		justifyContent: "flex-end",
	},
	perksButton: {
		backgroundColor: "#5875eb",
		marginTop: 20,
		marginBottom: 10,
		marginLeft: 20,
		marginRight: 20,
	},
});

function mapStateToProps(state) {
	return {
		getPerksRes: state.productReducer.getPerksRes,
	};
}
const mapDispatchToProps = {
	...productActions,
};
export default connect(mapStateToProps, mapDispatchToProps)(PerksScreen);
