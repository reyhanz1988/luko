import React, { useContext, useEffect, useRef, useState } from "react";
import { LangContext } from "../../App";
import { connect } from "react-redux";
import * as productActions from "../redux/actions/productActions";
import {
	ActivityIndicator,
	Animated,
	Dimensions,
	Easing,
	ScrollView,
	StyleSheet,
	View,
} from "react-native";
import { Appbar, Button, Card, Paragraph, Searchbar, Text, TextInput } from "react-native-paper";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import LangComponent from "../components/LangComponent";
import "intl";
import "intl/locale-data/jsonp/en";
import languages from "../screensTranslations/Realties";
import PropTypes from "prop-types";

const RealtiesScreen = (props) => {
	const { currentLang } = useContext(LangContext);
	const [getDataError, setGetDataError] = useState([]);
	const [typingTimeout, setTypingTimeout] = useState(false);
	const [searchQuery, setSearchQuery] = useState("");
	const [searchState, setSearchState] = useState(false);
	const [searchLoading, setSearchLoading] = useState(false);
	const [toggleSearch, setToggleSearch] = useState("");
	const [minPrice, setMinPrice] = useState("");
	const [maxPrice, setMaxPrice] = useState("");
	const [minBeds, setMinBeds] = useState("");
	const [maxBeds, setMaxBeds] = useState("");
	const [getRealties, setGetRealties] = useState([]);
	const fadeAnim = useRef(new Animated.Value(1)).current;

	//FIRST LOAD
	useEffect(() => {
		props.getRealties();
	}, []);
	//PROPS UPDATE
	useEffect(() => {
		if (props.getRealtiesRes == "error") {
			setGetDataError(props.getRealtiesRes);
		} else {
			if (props.getRealtiesRes.status_msg) {
				setGetDataError(props.getRealtiesRes.status_msg);
			} else {
				setGetRealties(props.getRealtiesRes);
				setGetDataError("");
			}
		}
	}, [props.getRealtiesRes]);
	const onChangeSearch = (query) => {
		if (typingTimeout) {
			clearTimeout(typingTimeout);
		}
		setSearchLoading(true);
		setSearchQuery(query);
		setSearchState(true);
	};
	const onChangeAdvanceSearch = (query) => {
		if (typingTimeout) {
			clearTimeout(typingTimeout);
		}
		setSearchQuery(query);
	};
	const advanceSearch = () => {
		setSearchLoading(true);
		setSearchState(true);
	};
	const resetSearch = () => {
		setSearchQuery("");
		setMinPrice("");
		setMaxPrice("");
		setMinBeds("");
		setMaxBeds("");
		setSearchLoading(true);
		setSearchState(true);
	};
	useEffect(() => {
		if (searchState) {
			setTypingTimeout(
				setTimeout(async () => {
					if (searchQuery.length == 0 || searchQuery.length >= 3) {
						let results = getRealties;
						if (searchQuery != "" && searchQuery.length >= 3) {
							results = getRealties.filter((res) => {
								setSearchState(false);
								return res.title.toLowerCase().includes(searchQuery.toLowerCase());
							});
						} else {
							results = props.getRealtiesRes;
							setSearchState(false);
						}
						if (results && minPrice && parseInt(minPrice) > 0) {
							results = results.filter((item) => {
								setSearchState(false);
								return item.price >= parseInt(minPrice);
							});
						}
						if (results && maxPrice && parseInt(maxPrice) > 0) {
							results = results.filter((item) => {
								setSearchState(false);
								return item.price <= parseInt(maxPrice);
							});
						}
						if (results && minBeds && parseInt(minBeds) > 0) {
							results = results.filter((item) => {
								setSearchState(false);
								return item.bedrooms >= parseInt(minBeds);
							});
						}
						if (results && maxBeds && parseInt(maxBeds) > 0) {
							results = results.filter((item) => {
								setSearchState(false);
								return item.bedrooms <= parseInt(maxBeds);
							});
						}
						setGetRealties(results);
						setSearchLoading(false);
					}
				}, 1500),
			);
		}
	}, [searchQuery, searchState]);
	const formatter = new Intl.NumberFormat("en-US", {
		style: "currency",
		currency: "EUR",
		maximumFractionDigits: 0,
		minimumFractionDigits: 0,
	});
	const animateSearch = () => {
		Animated.timing(fadeAnim, {
			toValue: -Dimensions.get("window").width * 1,
			duration: 250,
			easing: Easing.linear,
			useNativeDriver: true,
		}).start(() => {
			setToggleSearch(!toggleSearch);
			Animated.timing(fadeAnim, {
				toValue: 1,
				duration: 250,
				easing: Easing.linear,
				useNativeDriver: true,
			}).start();
		});
	};
	//RENDER
	if (getDataError != "") {
		props.getRealties();
	} else {
		let realtiesList = [];
		if (searchLoading) {
			realtiesList = <ActivityIndicator />;
		} else {
			if (getRealties.length > 0) {
				for (let i = 0; i < getRealties.length; i++) {
					realtiesList.push(
						<Card key={"realties_" + i} style={styles.realtiesCard}>
							<Card.Cover source={getRealties[i].image} />
							<Card.Content>
								<Paragraph style={styles.realtiesParagrapTitle}>
									{getRealties[i].title}
								</Paragraph>
								<Paragraph style={styles.realtiesPrice}>
									{formatter.format(getRealties[i].price)}
								</Paragraph>
								<Paragraph style={styles.realtiesFirstParagraph}>
									Region : {getRealties[i].region}
								</Paragraph>
								<Paragraph style={styles.realtiesParagraph}>
									Department : {getRealties[i].department}
								</Paragraph>
								<Paragraph style={styles.realtiesParagraph}>
									Location : {getRealties[i].location}
								</Paragraph>
								<View style={styles.realtiesIconsView}>
									<Text
										style={{
											color: "#666",
											borderRightWidth: 1,
											paddingRight: 10,
										}}>
										<MaterialCommunityIcons name="bed" size={16} /> :{" "}
										{getRealties[i].bedrooms.toString()}
									</Text>
									<Text
										style={{
											color: "#666",
											borderRightWidth: 1,
											paddingRight: 10,
											marginLeft: 10,
										}}>
										<MaterialCommunityIcons name="shower" size={16} /> :{" "}
										{getRealties[i].bathrooms.toString()}
									</Text>
									<Text
										style={{
											color: "#666",
											marginLeft: 10,
										}}>
										<MaterialCommunityIcons name="home" size={16} /> :{" "}
										{getRealties[i].property_size.toString()} sqm
									</Text>
								</View>
							</Card.Content>
							<Card.Actions style={styles.realtiesActions}>
								<Button
									style={styles.realtiesButton}
									mode="contained"
									onPress={() => console.log("Pressed")}>
									{languages[1][currentLang]}
								</Button>
							</Card.Actions>
						</Card>,
					);
				}
			}
		}

		let searchViews;
		if (toggleSearch) {
			searchViews = (
				<Animated.View
					testID="RealtiesAdvanceSearch"
					style={[{ transform: [{ translateY: fadeAnim }] }]}>
					<View style={styles.advanceSearchView}>
						<Searchbar
							testID="RealtiesAdvanceSearchBar"
							style={styles.searchbarStyle}
							placeholder={languages[2][currentLang]}
							onChangeText={(text) => onChangeAdvanceSearch(text)}
							value={searchQuery}
						/>
						<View style={styles.searchAttributes}>
							<TextInput
								testID="RealtiesAdvanceMinPrice"
								style={styles.searchAttributesInput}
								mode="outlined"
								keyboardType="numeric"
								label={languages[4][currentLang]}
								value={minPrice}
								onChangeText={(text) => setMinPrice(text)}
							/>
							<TextInput
								testID="RealtiesAdvanceMaxPrice"
								style={styles.searchAttributesInput}
								mode="outlined"
								keyboardType="numeric"
								label={languages[5][currentLang]}
								value={maxPrice}
								onChangeText={(text) => setMaxPrice(text)}
							/>
						</View>
						<View style={styles.searchAttributes}>
							<TextInput
								testID="RealtiesAdvanceMinBeds"
								style={styles.searchAttributesInput}
								mode="outlined"
								keyboardType="numeric"
								label={languages[7][currentLang]}
								value={minBeds}
								onChangeText={(text) => setMinBeds(text)}
							/>
							<TextInput
								testID="RealtiesAdvanceMaxBeds"
								style={styles.searchAttributesInput}
								mode="outlined"
								keyboardType="numeric"
								label={languages[8][currentLang]}
								value={maxBeds}
								onChangeText={(text) => setMaxBeds(text)}
							/>
						</View>
						<View style={styles.searchButtonsWrapper}>
							<Button
								testID="RealtiesFilterAdvanceSearch"
								style={styles.filterButton}
								mode="contained"
								onPress={() => advanceSearch()}>
								{languages[9][currentLang]}
							</Button>
							<Button
								testID="RealtiesResetAdvanceSearch"
								style={styles.resetSearchButton}
								mode="contained"
								onPress={() => resetSearch()}>
								{languages[10][currentLang]}
							</Button>
							<Button
								testID="RealtiesCloseAdvanceSearch"
								style={styles.closeSearchButton}
								mode="contained"
								onPress={() => animateSearch()}>
								{languages[11][currentLang]}
							</Button>
						</View>
					</View>
				</Animated.View>
			);
		} else {
			searchViews = (
				<Animated.View
					testID="RealtiesNormalSearch"
					style={[{ transform: [{ translateY: fadeAnim }] }]}>
					<View style={styles.advanceSearchView}>
						<Searchbar
							testID="RealtiesNormalSearchBar"
							style={styles.searchbarStyle}
							placeholder={languages[2][currentLang]}
							onChangeText={(text) => onChangeSearch(text)}
							value={searchQuery}
						/>
						<View style={styles.searchButtonsWrapper}>
							<Button
								testID="RealtiesResetNormalSearch"
								style={styles.resetSearchButton}
								mode="contained"
								onPress={() => resetSearch()}>
								{languages[10][currentLang]}
							</Button>
							<Button
								testID="RealtiesOpenAdvanceSearch"
								style={styles.searchButton}
								mode="contained"
								onPress={() => animateSearch()}>
								{languages[12][currentLang]}
							</Button>
						</View>
					</View>
				</Animated.View>
			);
		}
		return (
			<View style={styles.wrapper} testID="RealtiesScreen">
				<Appbar.Header style={{ zIndex: 20 }}>
					<Appbar.Content
						titleStyle={{ color: "#fff", textAlign: "center" }}
						title={languages[0][currentLang]}
					/>
					<LangComponent />
				</Appbar.Header>
				{searchViews}
				<ScrollView testID="RealtiesList" style={styles.scrollWrapper}>
					{realtiesList}
				</ScrollView>
			</View>
		);
	}
};

RealtiesScreen.propTypes = {
	getRealties: PropTypes.func,
	getRealtiesRes: PropTypes.array,
	navigation: PropTypes.shape({
		navigate: PropTypes.func,
	}),
};
type RealtiesScreen = PropTypes.InferProps<typeof propTypes>;

const styles = StyleSheet.create({
	wrapper: {
		flex: 1,
	},
	advanceSearchView: {
		backgroundColor: "#eae9e4",
		marginBottom: 10,
	},
	searchbarStyle: {
		marginTop: 10,
		marginBottom: 10,
		marginLeft: 20,
		marginRight: 20,
	},
	searchFilter: {
		marginLeft: 25,
	},
	searchAttributes: {
		flexDirection: "row",
		marginLeft: 20,
		marginRight: 20,
		justifyContent: "space-evenly",
		marginBottom: 10,
	},
	searchAttributesInput: {
		flex: 1,
		marginRight: 5,
		marginLeft: 5,
		height: 40,
	},
	searchButtonsWrapper: {
		flexDirection: "row",
		marginLeft: 20,
		marginRight: 20,
		marginBottom: 10,
		justifyContent: "space-evenly",
	},
	searchButton: {
		flex: 1,
		marginRight: 5,
		marginLeft: 5,
		backgroundColor: "#5875eb",
	},
	filterButton: {
		flex: 1,
		marginRight: 5,
		marginLeft: 5,
		backgroundColor: "#5875eb",
	},
	resetSearchButton: {
		marginRight: 5,
		marginLeft: 5,
		backgroundColor: "#2a8c7d",
	},
	closeSearchButton: {
		flex: 1,
		marginRight: 5,
		marginLeft: 5,
		backgroundColor: "red",
	},
	scrollWrapper: {
		paddingLeft: 20,
		paddingRight: 20,
		paddingBottom: 20,
		marginBottom: 20,
	},
	realtiesCard: {
		marginBottom: 20,
	},
	realtiesParagrapTitle: {
		fontSize: 16,
		fontWeight: "bold",
		marginTop: 10,
		textAlign: "justify",
		borderBottomWidth: 1,
		paddingBottom: 10,
		borderBottomColor: "#ddd",
	},
	realtiesFirstParagraph: {
		marginTop: 10,
		textAlign: "justify",
	},
	realtiesParagraph: {
		textAlign: "justify",
	},
	realtiesActions: {
		justifyContent: "flex-end",
	},
	realtiesIconsView: {
		flexDirection: "row",
		marginTop: 10,
	},
	realtiesPrice: {
		fontWeight: "bold",
		marginTop: 20,
		lineHeight: 24,
		fontSize: 24,
		color: "#2a8c7d",
	},
	realtiesButton: {
		backgroundColor: "#5875eb",
		margin: 10,
	},
});

function mapStateToProps(state) {
	return {
		getRealtiesRes: state.productReducer.getRealtiesRes,
	};
}
const mapDispatchToProps = {
	...productActions,
};
export default connect(mapStateToProps, mapDispatchToProps)(RealtiesScreen);
