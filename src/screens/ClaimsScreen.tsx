import React, { useContext, useEffect, useState } from "react";
import { LangContext } from "../../App";
import { connect } from "react-redux";
import * as productActions from "../redux/actions/productActions";
import { Dimensions, ScrollView, StyleSheet, View } from "react-native";
import { Card, Divider, Text } from "react-native-paper";
//import languages from "../screensTranslations/Claims";
import PropTypes from "prop-types";

const ClaimsScreen = (props) => {
	const { currentLang } = useContext(LangContext);
	const [getDataError, setGetDataError] = useState([]);
	const [getClaims, setGetClaims] = useState([]);
	//FIRST LOAD
	useEffect(() => {
		props.getClaims();
	}, []);
	//PROPS UPDATE
	useEffect(() => {
		if (props.getClaimsRes == "error") {
			setGetDataError(props.getClaimsRes);
		} else {
			if (props.getClaimsRes.status_msg) {
				setGetDataError(props.getClaimsRes.status_msg);
			} else {
				setGetClaims(props.getClaimsRes);
				setGetDataError("");
			}
		}
	}, [props.getClaimsRes]);
	//RENDER
	if (getDataError != "") {
		props.getClaims();
	} else {
		const claimsList = [];
		if (getClaims.length > 0) {
			for (const i in getClaims) {
				claimsList.push(
					<Card key={"claims_" + i} style={styles.claimsCard}>
						<Card.Title title={(parseInt(i)+1).toString()+". "+getClaims[i].title[currentLang]} />
						<Divider style={styles.theDivider} />
						<Card.Content>
							<Text style={styles.descriptionClaims}>
								{getClaims[i].description[currentLang]}
							</Text>
						</Card.Content>
					</Card>
				);
			}
		}
		return (
			<View style={styles.wrapper} testID="ClaimsScreen">
				<ScrollView style={styles.scrollWrapper}>{claimsList}</ScrollView>
			</View>
		);
	}
};

ClaimsScreen.propTypes = {
	getClaims: PropTypes.func,
	getClaimsRes: PropTypes.array,
	navigation: PropTypes.shape({
		navigate: PropTypes.func,
	}),
};
type ClaimsScreen = PropTypes.InferProps<typeof propTypes>;

const styles = StyleSheet.create({
	wrapper: {
		flex: 1,
	},
	claimsHeader: {
		flexDirection: "row",
		width: Dimensions.get("window").width * 0.5,
		backgroundColor: "#56a497",
		padding: 10,
		marginTop: 20,
		marginLeft: 20,
		marginRight: 20,
		borderRadius: 5,
	},
	claimsHeaderSelected: {
		flexDirection: "row",
		width: Dimensions.get("window").width * 0.5,
		backgroundColor: "#5875eb",
		padding: 10,
		marginTop: 20,
		marginLeft: 20,
		marginRight: 20,
		borderRadius: 5,
	},
	scrollWrapper: {
		padding: 20,
		marginBottom: 20,
	},
	claimsHeaderText: {
		color: "#fff",
		fontSize: 16,
		fontWeight: "bold",
	},
	claimsHeaderIcon: {
		color: "#fff",
		fontSize: 16,
		fontWeight: "bold",
	},
	claimsCard: {
		marginBottom: 20,
	},
	theDivider: {
		backgroundColor: "#a6d3cc",
		height: 2,
		marginLeft: 10,
		marginRight: 10,
		marginBottom: 10,
	},
	contentClaims: {
		color: "#666",
		fontWeight: "bold",
		fontSize: 16,
		marginBottom: 10,
	},
	descriptionClaims: {
		color: "#666",
		textAlign: "justify",
	},
	claimActions: {
		justifyContent: "flex-end",
	},
});

function mapStateToProps(state) {
	return {
		getClaimsRes: state.productReducer.getClaimsRes,
	};
}
const mapDispatchToProps = {
	...productActions,
};
export default connect(mapStateToProps, mapDispatchToProps)(ClaimsScreen);
