import React, { useContext } from "react";
import { LangContext } from "../../App";
import { StyleSheet, View } from "react-native";
import { Appbar } from "react-native-paper";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import ProductsScreen from "./ProductsScreen";
import PerksScreen from "./PerksScreen";
import ClaimsScreen from "./ClaimsScreen";
import LangComponent from "../components/LangComponent";
import languages from "../screensTranslations/Insurance";
import PropTypes from "prop-types";

const Tab = createMaterialTopTabNavigator();

const InsuranceScreen = () => {
	const { currentLang } = useContext(LangContext);
	//RENDER
	return (
		<View style={styles.wrapper} testID="InsuranceScreen">
			<Appbar.Header>
				<Appbar.Content
					titleStyle={{ color: "#fff", textAlign: "center" }}
					title={languages[0][currentLang]}
				/>
				<LangComponent />
			</Appbar.Header>
			<Tab.Navigator
				initialRouteName="Products"
				screenOptions={{
					tabBarLabelStyle: {
						fontSize: 16,
						color: "#000",
						textTransform: "none",
					},
				}}>
				<Tab.Screen
					name="Products"
					component={ProductsScreen}
					options={{
						tabBarTestID: "ProductsNav",
						tabBarLabel: languages[1][currentLang],
					}}
				/>
				<Tab.Screen
					name="Perks"
					component={PerksScreen}
					options={{
						tabBarTestID: "PerksNav",
						tabBarLabel: languages[2][currentLang],
					}}
				/>
				<Tab.Screen
					name="Claims"
					component={ClaimsScreen}
					options={{
						tabBarTestID: "ClaimsNav",
						tabBarLabel: languages[3][currentLang],
					}}
				/>
			</Tab.Navigator>
		</View>
	);
};

InsuranceScreen.propTypes = {
	navigation: PropTypes.shape({
		navigate: PropTypes.func,
	}),
};
type InsuranceScreen = PropTypes.InferProps<typeof propTypes>;

const styles = StyleSheet.create({
	wrapper: {
		flex: 1,
	},
});

export default InsuranceScreen;
