import React, { useContext, useEffect, useState } from "react";
import { LangContext } from "../../App";
import { connect } from "react-redux";
import * as productActions from "../redux/actions/productActions";
import { ScrollView, StyleSheet, View } from "react-native";
import { Button, Card, Paragraph } from "react-native-paper";
import languages from "../screensTranslations/Products";
import PropTypes from "prop-types";

const ProductsScreen = (props) => {
	const { currentLang } = useContext(LangContext);
	const [getDataError, setGetDataError] = useState([]);
	const [getProducts, setGetProducts] = useState([]);
	//FIRST LOAD
	useEffect(() => {
		props.getProducts();
	}, []);
	//PROPS UPDATE
	useEffect(() => {
		if (props.getProductsRes == "error") {
			setGetDataError(props.getProductsRes);
		} else {
			if (props.getProductsRes.status_msg) {
				setGetDataError(props.getProductsRes.status_msg);
			} else {
				setGetProducts(props.getProductsRes);
				setGetDataError("");
			}
		}
	}, [props.getProductsRes]);
	//RENDER
	if (getDataError != "") {
		props.getProducts();
	} else {
		const productsList = [];
		if (getProducts.length > 0) {
			for (let i = 0; i < getProducts.length; i++) {
				productsList.push(
					<Card key={"products_" + i} style={styles.productsCard}>
						<Card.Title title={getProducts[i].title[currentLang]} />
						<Card.Cover source={getProducts[i].image} />
						<Card.Content>
							<Paragraph style={styles.productsParagraph}>
								{getProducts[i].description[currentLang]}
							</Paragraph>
						</Card.Content>
						<Card.Actions style={styles.productsActions}>
							<Button
								style={styles.productsButton}
								mode="contained"
								onPress={() => console.log("Pressed")}>
								{languages[1][currentLang]}
							</Button>
						</Card.Actions>
					</Card>
				);
			}
		}
		return (
			<View style={styles.wrapper} testID="ProductsScreen">
				<ScrollView style={styles.scrollWrapper}>{productsList}</ScrollView>
			</View>
		);
	}
};

ProductsScreen.propTypes = {
	getProducts: PropTypes.func,
	getProductsRes: PropTypes.array,
	navigation: PropTypes.shape({
		navigate: PropTypes.func,
	}),
};
type ProductsScreen = PropTypes.InferProps<typeof propTypes>;

const styles = StyleSheet.create({
	wrapper: {
		flex: 1,
	},
	scrollWrapper: {
		padding: 20,
		marginBottom: 20,
	},
	productsCard: {
		marginBottom: 20,
	},
	productsParagraph: {
		marginTop: 10,
		textAlign: "justify",
	},
	productsActions: {
		justifyContent: "flex-end",
	},
	productsButton: {
		backgroundColor: "#5875eb",
		margin: 10,
	},
});

function mapStateToProps(state) {
	return {
		getProductsRes: state.productReducer.getProductsRes,
	};
}
const mapDispatchToProps = {
	...productActions,
};
export default connect(mapStateToProps, mapDispatchToProps)(ProductsScreen);
