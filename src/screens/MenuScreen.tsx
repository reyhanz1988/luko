import React, { useContext, useEffect, useState } from "react";
import { LoginContext, LangContext } from "../../App";
import { connect } from "react-redux";
import * as accountActions from "../redux/actions/accountActions";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { ScrollView, StyleSheet, View, Text } from "react-native";
import { ActivityIndicator, Appbar, Avatar, Divider, List } from "react-native-paper";
import languages from "../screensTranslations/Menu";
import LangComponent from "../components/LangComponent";
import PropTypes from "prop-types";

const MenuScreen = (props) => {
	const { token } = useContext(LoginContext);
	const { setToken } = useContext(LoginContext);
	const { currentLang } = useContext(LangContext);
	const [loading, setLoading] = useState(true);
	const [user, setUser] = useState([]);
	const logoutRedux = () => {
		const vars = {};
		vars.token = token;
		vars.currentLang = currentLang;
		props.logoutRedux(vars);
	};
	//DID MOUNT
	useEffect(() => {
		const vars = {};
		vars.token = token;
		props.getUser(vars);
	}, []);
	//PROPS UPDATE USER
	useEffect(() => {
		setUser(props.getUserRes);
		setLoading(false);
	}, [props.getUserRes]);
	//PROPS UPDATE
	useEffect(() => {
		if (props.logoutReduxRes && props.logoutReduxRes.status == "success") {
			AsyncStorage.clear().then(() => {
				setToken(null);
				const vars = {};
				vars.token = null;
				props.getUser(vars);
			});
		}
	}, [props.logoutReduxRes]);
	let menuList;
	if (token == null) {
		menuList = (
			<List.Item
				title={"Login"}
				left={(props) => <List.Icon {...props} icon={"login"} />}
				right={(props) => <List.Icon {...props} icon="chevron-right" />}
				onPress={() => props.navigation.navigate("Login")}
				style={{ borderBottomWidth: 1, borderBottomColor: "#ddd" }}
			/>
		);
	} else {
		menuList = (
			<List.Item
				title={"Logout"}
				left={(props) => <List.Icon {...props} icon={"logout"} />}
				right={(props) => <List.Icon {...props} icon="chevron-right" />}
				onPress={() => logoutRedux()}
				style={{ borderBottomWidth: 1, borderBottomColor: "#ddd" }}
			/>
		);
	}
	if (loading) {
		//LOADING
		return (
			<View style={styles.wrapper} testID="MenuLoading">
				<ActivityIndicator size="large" />
			</View>
		);
	} else {
		//RENDER
		let userData = [];
		if (user) {
			userData = (
				<View style={styles.userView}>
					<Avatar.Image size={64} source={user.photo} />
					<Text style={styles.userText}>{user.name}</Text>
				</View>
			);
		}
		return (
			<View style={styles.wrapper} testID="MenuScreen">
				<Appbar.Header>
					<Appbar.Content
						titleStyle={{ color: "#fff", textAlign: "center" }}
						title={languages[0][currentLang]}
					/>
					<LangComponent />
				</Appbar.Header>
				{userData}
				<Divider style={styles.theDivider} />
				<ScrollView style={{ marginLeft: 10, marginRight: 10 }}>
					<List.Item
						title={languages[4][currentLang]}
						left={(props) => (
							<List.Icon {...props} icon={"frequently-asked-questions"} />
						)}
						right={(props) => <List.Icon {...props} icon="chevron-right" />}
						onPress={() => console.log("faq pressed")}
						style={{ borderBottomWidth: 1, borderBottomColor: "#ddd" }}
					/>
					<List.Item
						title={languages[5][currentLang]}
						left={(props) => <List.Icon {...props} icon={"gavel"} />}
						right={(props) => <List.Icon {...props} icon="chevron-right" />}
						onPress={() => console.log("privacy policy pressed")}
						style={{ borderBottomWidth: 1, borderBottomColor: "#ddd" }}
					/>
					{menuList}
				</ScrollView>
			</View>
		);
	}
};

MenuScreen.propTypes = {
	getUser: PropTypes.func,
	getUserRes: PropTypes.any,
	logoutRedux: PropTypes.func,
	logoutReduxRes: PropTypes.any,
	navigation: PropTypes.shape({
		navigate: PropTypes.func,
	}),
};
type MenuScreen = PropTypes.InferProps<typeof propTypes>;

const styles = StyleSheet.create({
	wrapper: {
		flex: 1,
	},
	userView: {
		flexDirection: "row",
		justifyContent: "center",
		margin: 10,
		padding: 10,
	},
	userText: {
		flex: 1,
		justifyContent: "center",
		marginLeft: 20,
		marginTop: 15,
		fontSize: 24,
	},
	theDivider: {
		backgroundColor: "#a6d3cc",
		height: 2,
		marginLeft: 10,
		marginRight: 10,
		marginBottom: 10,
	},
});

function mapStateToProps(props) {
	return {
		getUserRes: props.accountReducer.getUserRes,
		logoutReduxRes: props.accountReducer.logoutReduxRes,
	};
}
const mapDispatchToProps = {
	...accountActions,
};
export default connect(mapStateToProps, mapDispatchToProps)(MenuScreen);
