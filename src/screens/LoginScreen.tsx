import React, { useContext, useEffect, useState } from "react";
import { LoginContext, LangContext } from "../../App";
import { connect } from "react-redux";
import * as accountActions from "../redux/actions/accountActions";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Image, StyleSheet, View } from "react-native";
import { Appbar, Button, Text, TextInput } from "react-native-paper";
import logo from "../assets/images/logo.png";
import { FIRST_COLOR, SECOND_COLOR } from "@env";
import languages from "../screensTranslations/Login";
import LangComponent from "../components/LangComponent";
import PropTypes from "prop-types";

const LoginScreen = (props) => {
	const { setToken } = useContext(LoginContext);
	const { currentLang } = useContext(LangContext);
	const [email, setEmail] = React.useState("");
	const [password, setPassword] = React.useState("");
	const [getDataError, setGetDataError] = useState(null);
	const [user, setUser] = useState([]);
	const checkLogin = () => {
		const vars = {};
		vars.currentLang = currentLang;
		vars.email = email;
		vars.password = password;
		props.checkLogin(vars);
	};
	//PROPS UPDATE LOGIN
	useEffect(() => {
		if (props.checkLoginRes) {
			if (props.checkLoginRes.status == "error") {
				setGetDataError(props.checkLoginRes.msg);
				setTimeout(async () => {
					setGetDataError(null);
				}, 3000);
			} else {
				const vars = {};
				vars.token = props.checkLoginRes.token;
				props.getUser(vars);
				AsyncStorage.setItem("TOKEN", props.checkLoginRes.token).then(() => {
					setGetDataError(null);
					setToken(props.checkLoginRes.token);
					props.navigation.goBack();
				});
			}
		}
	}, [props.checkLoginRes]);
	let errorView;
	if (getDataError) {
		errorView = (
			<Text testID="loginError" style={styles.error}>
				{getDataError}
			</Text>
		);
	}
	//RENDER
	return (
		<View style={styles.wrapper} testID="LoginScreen">
			<Appbar.Header>
				<Appbar.Action icon="arrow-left" onPress={() => props.navigation.goBack()} />
				<Appbar.Content
					titleStyle={{ color: "#fff", textAlign: "center" }}
					title={languages[3][currentLang]}
				/>
				<LangComponent />
			</Appbar.Header>
			<View style={styles.logoView}>
				<Image source={logo} style={styles.logo} />
			</View>
			<View style={styles.loginView}>
				<TextInput
					testID="emailInput"
					type="outlined"
					style={styles.textInput}
					autoCapitalize="none"
					keyboardType={"email-address"}
					label={languages[1][currentLang]}
					placeholder={languages[1][currentLang]}
					value={email}
					onChangeText={(text) => setEmail(text)}
				/>
				<TextInput
					testID="passwordInput"
					type="outlined"
					style={styles.textInput}
					autoCapitalize="none"
					secureTextEntry={true}
					label={languages[2][currentLang]}
					placeholder={languages[2][currentLang]}
					value={password}
					onChangeText={(text) => setPassword(text)}
				/>
				<Button
					testID="loginButton"
					uppercase={false}
					style={styles.loginButton}
					mode="contained"
					onPress={() => {
						checkLogin();
					}}>
					{languages[3][currentLang]}
				</Button>
				{errorView}
			</View>
		</View>
	);
};

LoginScreen.propTypes = {
	getUser: PropTypes.func,
	getUserRes: PropTypes.any,
	checkLogin: PropTypes.func,
	checkLoginRes: PropTypes.any,
	navigation: PropTypes.shape({
		navigate: PropTypes.func,
		goBack: PropTypes.func,
	}),
};
type LoginScreen = PropTypes.InferProps<typeof propTypes>;

const styles = StyleSheet.create({
	wrapper: {
		flex: 1,
		backgroundColor: FIRST_COLOR,
	},
	logoView: {
		marginTop: 40,
		marginBottom: 20,
		alignItems: "center",
		justifyContent: "center",
	},
	langView: {
		flexDirection: "row",
		height: 30,
		marginBottom: 20,
		justifyContent: "center",
	},
	languageText: {
		marginLeft: 5,
		flex: 1,
		textAlign: "left",
	},
	logo: {
		resizeMode: "contain",
		height: 60,
	},
	loginView: {
		borderRadius: 10,
		padding: 20,
		margin: 20,
		backgroundColor: SECOND_COLOR,
	},
	textInput: {
		marginBottom: 20,
	},
	loginButton: {
		height: 60,
		justifyContent: "center",
	},
	error: {
		backgroundColor: "red",
		color: "#ffffff",
		padding: 10,
		borderRadius: 5,
		marginTop: 10,
	},
});

function mapStateToProps(props) {
	return {
		getUserRes: props.accountReducer.getUserRes,
		checkLoginRes: props.accountReducer.checkLoginRes,
	};
}
const mapDispatchToProps = {
	...accountActions,
};
export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
