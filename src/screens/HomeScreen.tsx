import React, { useContext, useState } from "react";
import { LangContext } from "../../App";
import { Dimensions, Image, StyleSheet, ScrollView, View } from "react-native";
import {
	Appbar,
	Button,
	Divider,
	FAB,
	IconButton,
	Portal,
	Provider,
	Surface,
	Text,
} from "react-native-paper";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import LangComponent from "../components/LangComponent";
import languages from "../screensTranslations/Home";
import { FIRST_COLOR } from "@env";

const HomeScreen = () => {
	const { currentLang } = useContext(LangContext);
	const [toggleFAB, setToggleFAB] = useState(false);
	//RENDER
	return (
		<View style={styles.wrapper} testID="HomeScreen">
			<Appbar.Header>
				<Appbar.Content
					titleStyle={{ color: "#fff", textAlign: "center" }}
					title={languages[0][currentLang]}
				/>
				<LangComponent />
			</Appbar.Header>
			<ScrollView>
				<View style={styles.powerGasView}>
					<Surface style={styles.powerGasSurface}>
						<Text style={styles.powerGasText}>
							<MaterialCommunityIcons name="lightning-bolt" size={24} />
							{languages[1][currentLang]}
						</Text>
						<View style={styles.powerGasIcon}>
							<IconButton
								icon="plus-circle"
								color={"#bfbeb9"}
								size={64}
								onPress={() => console.log("Pressed")}
							/>
						</View>
					</Surface>
					<Surface style={styles.powerGasSurface}>
						<Text style={styles.powerGasText}>
							<MaterialCommunityIcons name="fire" size={24} />
							{languages[2][currentLang]}
						</Text>
						<View style={styles.powerGasIcon}>
							<IconButton
								icon="plus-circle"
								color={"#bfbeb9"}
								size={64}
								onPress={() => console.log("Pressed")}
							/>
						</View>
					</Surface>
				</View>
				<View style={styles.boxView}>
					<Surface style={styles.newsSurface}>
						<Text style={styles.textHeader}>{languages[3][currentLang]}</Text>
						<Divider style={styles.theDivider} />
						<Text style={styles.newsHeader}>{languages[4][currentLang]}</Text>
						<Text style={styles.boxDescription}>{languages[5][currentLang]}</Text>
						<View style={styles.buttonView}>
							<Button
								uppercase={false}
								style={styles.newsButton}
								mode="contained"
								onPress={() => console.log("Pressed")}>
								{languages[6][currentLang]}
							</Button>
						</View>
						<Image
							style={styles.boxImage}
							source={require("../assets/images/home-news.jpg")}
						/>
					</Surface>
				</View>
				<View style={[styles.boxView, { marginBottom: 30 }]}>
					<Surface style={styles.realtySurface}>
						<Text style={styles.textHeader}>{languages[7][currentLang]}</Text>
						<Divider style={styles.theDivider} />
						<Text style={styles.boxDescription}>{languages[8][currentLang]}</Text>
						<View style={styles.buttonView}>
							<Button
								uppercase={false}
								style={styles.realtyButton}
								mode="contained"
								onPress={() => console.log("Pressed")}>
								{languages[9][currentLang]}
							</Button>
						</View>
						<Image
							style={styles.boxImage}
							source={require("../assets/images/home-realty.jpg")}
						/>
					</Surface>
				</View>
			</ScrollView>
			<Provider>
				<Portal>
					<FAB.Group
						open={toggleFAB}
						icon={toggleFAB ? "chevron-up" : "plus"}
						color={"#ffffff"}
						fabStyle={{ backgroundColor: FIRST_COLOR }}
						actions={[
							{
								icon: "fire",
								color: FIRST_COLOR,
								label: languages[10][currentLang],
								labelTextColor: FIRST_COLOR,
								onPress: () => console.log("Pressed Gas with Gazpar"),
							},
							{
								icon: "lightning-bolt",
								color: FIRST_COLOR,
								label: languages[11][currentLang],
								labelTextColor: FIRST_COLOR,
								onPress: () => console.log("Pressed Power with Linky"),
							},
							{
								icon: "water",
								color: FIRST_COLOR,
								label: languages[12][currentLang],
								labelTextColor: FIRST_COLOR,
								onPress: () => console.log("Pressed Luko Water"),
							},
						]}
						onStateChange={() => setToggleFAB(!toggleFAB)}
					/>
				</Portal>
			</Provider>
		</View>
	);
};

const styles = StyleSheet.create({
	wrapper: {
		flex: 1,
	},
	powerGasView: {
		flex: 1,
		flexDirection: "row",
		justifyContent: "space-evenly",
		marginTop: Dimensions.get("window").width * 0.05,
	},
	powerGasSurface: {
		width: Dimensions.get("window").width * 0.45,
		backgroundColor: "#eae9e4",
		height: Dimensions.get("window").width * 0.45,
		borderRadius: 10,
	},
	powerGasText: {
		width: Dimensions.get("window").width * 0.45,
		position: "absolute",
		textAlign: "center",
		top: 20,
		fontSize: 16,
		color: "#bfbeb9",
	},
	powerGasIcon: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center",
	},
	titlesView: {
		backgroundColor: FIRST_COLOR,
		borderTopRightRadius: 25,
		borderBottomRightRadius: 25,
		width: Dimensions.get("window").width * 0.3,
		marginTop: Dimensions.get("window").width * 0.05,
		marginLeft: Dimensions.get("window").width * 0.05,
	},
	titlesText: {
		color: "#fff",
		fontSize: 16,
		padding: 10,
	},
	boxView: {
		flex: 1,
		alignItems: "center",
		marginTop: Dimensions.get("window").width * 0.05,
	},
	textHeader: {
		fontSize: 24,
		fontWeight: "bold",
		marginTop: 10,
		marginLeft: 10,
		color: "#fff",
	},
	theDivider: {
		backgroundColor: "#a6d3cc",
		height: 2,
		width: Dimensions.get("window").width * 0.2,
		marginTop: 5,
		marginLeft: 10,
	},
	boxDescription: {
		fontSize: 16,
		margin: 10,
		color: "#a6d3cc",
	},
	buttonView: {
		flexWrap: "wrap",
		margin: 10,
	},
	boxImage: {
		width: Dimensions.get("window").width * 0.94,
		height: Dimensions.get("window").width * 0.4,
		resizeMode: "contain",
	},
	newsSurface: {
		width: Dimensions.get("window").width * 0.94,
		backgroundColor: "#2a8c7d",
		borderRadius: 10,
	},
	newsHeader: {
		fontSize: 18,
		marginTop: 10,
		marginLeft: 10,
		color: "#fff",
	},
	newsButton: {
		backgroundColor: "#56a497",
	},
	realtySurface: {
		width: Dimensions.get("window").width * 0.94,
		backgroundColor: FIRST_COLOR,
		borderRadius: 10,
	},
	realtyButton: {
		backgroundColor: "#5875eb",
	},
	plusAbsoluteButton: {
		position: "absolute",
		bottom: 20,
		right: 20,
		flexWrap: "wrap",
		backgroundColor: "#fff",
		borderRadius: 50,
	},
});

export default HomeScreen;
