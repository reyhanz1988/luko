import React, { createContext, useEffect, useLayoutEffect, useState } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { LogBox, Platform, StatusBar, StyleSheet, View } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import AppNav from "./src/navigation/AppNav";
import { DefaultTheme, Provider as PaperProvider, Portal } from "react-native-paper";
import { Provider as StoreProvider } from "react-redux";
import store from "./store";
import Orientation from "react-native-orientation-locker";
import { FIRST_COLOR, SECOND_COLOR } from "@env";

LogBox.ignoreLogs(["react-native-gesture-handler"]);

export const LoginContext = createContext(null);
export const LangContext = createContext("en");

const App: () => Node = () => {
	const theme = {
		...DefaultTheme,
		roundness: 5,
		colors: {
			...DefaultTheme.colors,
			primary: FIRST_COLOR,
			accent: SECOND_COLOR,
		},
	};

	const styles = StyleSheet.create({
		container: {
			flex: 1,
			backgroundColor: "#fff",
		},
	});
	const [token, setToken] = useState();
	const [currentLang, setCurrentLang] = useState("en");
	useLayoutEffect(() => {
		if (Orientation && typeof Orientation == "array") {
			Orientation.lockToPortrait();
		}
	}, []);
	useEffect(() => {
		//ignore asyncstorage error null
		LogBox.ignoreLogs(["Possible Unhandled Promise"]);
		LogBox.ignoreAllLogs();
		AsyncStorage.getItem("TOKEN").then((value) => {
			if (value !== null) {
				setToken(value);
			}
		});
		AsyncStorage.getItem("LANG").then((value) => {
			if (value == null) {
				AsyncStorage.setItem("LANG", "en").then(() => {
					setCurrentLang("en");
				});
			} else {
				setCurrentLang(value);
			}
		});
	}, []);

	return (
		<StoreProvider store={store}>
			<View style={styles.container}>
				<PaperProvider theme={theme}>
					<Portal>
						<LoginContext.Provider value={{ token, setToken }}>
							<LangContext.Provider value={{ currentLang, setCurrentLang }}>
								{Platform.OS === "ios" && <StatusBar />}
								<NavigationContainer>
									<AppNav />
								</NavigationContainer>
							</LangContext.Provider>
						</LoginContext.Provider>
					</Portal>
				</PaperProvider>
			</View>
		</StoreProvider>
	);
};
export default App;
